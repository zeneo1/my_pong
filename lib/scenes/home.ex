defmodule MyPong.Scene.Home do
  use Scenic.Scene, name: __MODULE__
  require Logger

  alias Scenic.Graph

  import Scenic.Primitives

  @vp_size Application.fetch_env!(:my_pong, :game).size




  def init(_, _opts) do
    graph =
      Graph.build(font_size: 40)
      |> rectangle({20, 100}, fill: :white, translate: {20, (@vp_size.height - 100) / 2}, id: :player)
      |> rectangle({20, 100}, fill: :white, translate: {@vp_size.width - 40, (@vp_size.height - 100) / 2}, id: :ai)
      |> rectangle({15, 15}, fill: :white, translate: {(@vp_size.width - 20) / 2, (@vp_size.height - 20) / 2}, id: :ball)

    graph = 0..10
    |> Enum.reduce(graph, fn elem, acc ->
      acc
      |> rectangle({5, @vp_size.height / 20}, fill: :white, translate: {(@vp_size.width - 5) / 2, (@vp_size.height / 20) * (2*elem + 0.5)})
    end)

    graph =
      graph
      |> text("0", fill: :white, font: :roboto_mono, translate: {(@vp_size.width - 150) / 2, 40}, id: :player_score)
      |> text("0", fill: :white, font: :roboto_mono, translate: {(@vp_size.width + 100) / 2, 40}, id: :machine_score)

    {:ok, %{graph: graph}, push: graph}
  end

  def handle_cast({:change_player_position, position}, state) do
    graph = state.graph
    |> Graph.modify(:player, &rectangle(&1, {20, 100}, translate: {20, position}))
    {:noreply, %{state | graph: graph}, push: graph}
  end

  def handle_cast({:change_ai_position, position}, state) do
    graph = state.graph
    |> Graph.modify(:ai, &rectangle(&1, {20, 100}, translate: {@vp_size.width - 40, position}))
    {:noreply, %{state | graph: graph}, push: graph}
  end

  def handle_cast({:change_ball_position, position}, state) do
    graph = state.graph
    |> Graph.modify(:ball, &rectangle(&1, {15, 15}, translate: position))
    {:noreply, %{state | graph: graph}, push: graph}
  end


  def handle_cast({:change_player_score, score}, state) do
    graph = state.graph
    |> Graph.modify(:player_score, &text(&1, Integer.to_string(score), translate: {(@vp_size.width - 150) / 2, 40}))
    {:noreply, %{state | graph: graph}, push: graph}
  end

  def handle_cast({:change_machine_score, score}, state) do
    graph = state.graph
    |> Graph.modify(:machine_score, &text(&1, Integer.to_string(score), translate: {(@vp_size.width + 100) / 2, 40}))
    {:noreply, %{state | graph: graph}, push: graph}
  end


  def handle_input(event, _context, state) do
    GenServer.cast(MyPong.Player, event)
    {:noreply, state}
  end


end
