defmodule MyPong.Scene.Home.Helper do
  alias MyPong.Scene.Home

  def change_player_position(position) do
    GenServer.cast(Home, {:change_player_position, position})
  end

  def change_ai_position(position) do
    GenServer.cast(Home, {:change_ai_position, position})
  end

  def change_ball_position(%{x: x, y: y}) do
    GenServer.cast(Home, {:change_ball_position, {x, y}})
  end

  def change_player_score(score) do
    GenServer.cast(Home, {:change_player_score, score})
  end

  def change_machine_score(score) do
    GenServer.cast(Home, {:change_machine_score, score})
  end

end
