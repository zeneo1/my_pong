defmodule MyPong do
  @moduledoc """
  Starter application using the Scenic framework.
  """

  def start(_type, _args) do
    # load the viewport configuration from config
    main_viewport_config = Application.get_env(:my_pong, :viewport)

    # start the application with the viewport
    children = [
      {Scenic, viewports: [main_viewport_config]},
      {MyPong.Game, []},
      {MyPong.Player, []},
      {MyPong.Ball, []},
      {MyPong.Machine, []}
    ]

    Supervisor.start_link(children, strategy: :one_for_one)
  end
end
