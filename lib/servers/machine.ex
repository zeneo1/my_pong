defmodule MyPong.Machine do
  use GenServer

  alias MyPong.Scene.Home.Helper

  # @vp_size Application.fetch_env!(:my_pong, :game).size
  @machine __MODULE__
  @fps 60

  def start_link(_) do
    GenServer.start_link(@machine, nil, name: @machine)
  end

  def init(arg) do
    1000 / @fps
    |> trunc()
    |> :timer.send_interval(:move)
    {:ok, arg}
  end

  def handle_info(:move, state) do
    (MyPong.Ball.get_y() - 40)
    |> Helper.change_ai_position()
    {:noreply, state}
  end


end
