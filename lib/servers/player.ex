defmodule MyPong.Player do
  use GenServer

  alias MyPong.Scene.Home.Helper

  @vp_size Application.fetch_env!(:my_pong, :game).size
  @player __MODULE__
  @fps Application.fetch_env!(:my_pong, :game).fps
  @pixels_per_move 1

  defstruct [
    position: (@vp_size.height - 100) / 2,
    timer_up: nil,
    timer_down: nil
  ]

  def start_link(_) do
    GenServer.start_link(@player, %@player{}, name: @player)
  end

  def init(args) do
    {:ok, args}
  end

  def handle_call(:position, _from, state) do
    {:reply, state.position, state}
  end

  def handle_cast({:key, {"up", :press, 0}}, state) do
    {:ok, timer} = 1000 / @fps
    |> trunc()
    |> :timer.send_interval(:up)
    {:noreply, %{state | timer_up: timer}}
  end


  def handle_cast({:key, {"up", :release, 0}}, state) do
    :timer.cancel(state.timer_up)
    {:noreply, state}
  end

  def handle_cast({:key, {"down", :press, 0}}, state) do
    {:ok, timer} = 1000 / @fps
    |> trunc()
    |> :timer.send_interval(:down)
    {:noreply, %{state | timer_down: timer}}
  end


  def handle_cast({:key, {"down", :release, 0}}, state) do
    :timer.cancel(state.timer_down)
    {:noreply, state}
  end

  def handle_cast(_, state) do
    {:noreply, state}
  end

  def handle_info(:up, state = %{position: position}) when position - @pixels_per_move >= 0 do
    Helper.change_player_position(position - @pixels_per_move)
    {:noreply, %{state | position: position - @pixels_per_move}}
  end

  def handle_info(:up, state)do
    {:noreply, state}
  end

  def handle_info(:down, state = %{position: position}) when position + 100 + @pixels_per_move <= @vp_size.height  do
    Helper.change_player_position(position + @pixels_per_move)
    {:noreply, %{state | position: position + @pixels_per_move}}
  end


  def handle_info(:down, state) do
    {:noreply, state}
  end


  def get_position do
    GenServer.call(@player, :position)
  end

end
