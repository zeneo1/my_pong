defmodule MyPong.Game do
  use GenServer

  defstruct player: 0, machine: 0

  alias MyPong.Scene.Home.Helper

  @game __MODULE__

  def start_link(_) do
    GenServer.start_link(@game, %@game{}, name: @game)
  end

  def init(arg) do
    {:ok, arg}
  end


  def handle_cast(:player_point, state) do
    Helper.change_player_score(state.player + 1)
    {:noreply, %{state | player: state.player + 1}}
  end


  def handle_cast(:machine_point, state) do
    Helper.change_machine_score(state.machine + 1)
    {:noreply, %{state | machine: state.machine + 1}}
  end


  def player_point do
    GenServer.cast(@game, :player_point)
  end


  def machine_point do
    GenServer.cast(@game, :machine_point)
  end


end
