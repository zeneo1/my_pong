defmodule MyPong.Ball do
  use GenServer

  alias MyPong.Scene.Home.Helper
  alias MyPong.Player
  alias MyPong.Game

  @vp_size Application.fetch_env!(:my_pong, :game).size
  @ball __MODULE__
  @fps Application.fetch_env!(:my_pong, :game).fps



  defstruct [
    dx: 0,
    dy: 0,
    dxd: 0,
    dyd: 0,
    x: 0,
    y: 0
  ]


  def start_link(_) do
    GenServer.start_link(@ball, generate_state(), name: @ball)
  end

  def init(arg) do
    1000 / @fps
    |> trunc()
    |> :timer.send_interval(:move)
    {:ok, arg}
  end

  def handle_call(:y, _from, state) do
    {:reply, state.y, state}
  end

  # vertical limit
  def handle_info(:move, state)
  when (state.y >= @vp_size.height - 20 and state.dyd == 1) or (state.y <= 0 and state.dyd == 1) do
    state = %{state | dyd: 0} |> move_ball()
    state |> Helper.change_ball_position()
    {:noreply, state}
  end

  def handle_info(:move, state)
  when (state.y >= @vp_size.height - 20 and state.dyd == 0) or (state.y <= 0 and state.dyd == 0) do
    state = %{state | dyd: 1} |> move_ball()
    state |> Helper.change_ball_position()
    {:noreply, state}
  end



  # horizontal limit
  def handle_info(:move, state)
  when state.x >= @vp_size.width - 40 and state.dxd == 1 do
    Game.player_point()
    (state = generate_state())
    |> Helper.change_ball_position()
    {:noreply, state}
  end

  def handle_info(:move, state)
  when state.x <= 20 and state.dxd == 0 do
    Game.machine_point()
    (state = generate_state())
    |> Helper.change_ball_position()
    {:noreply, state}
  end


  def handle_info(:move, state)
  when (state.x <= 40 and state.dxd == 0) do
    position = Player.get_position()
    state = if (state.y < position + 100 && state.y > position) do
              %{state | dx: 1, dy: 1..10 |> Enum.random, dxd: 1}
            else
              state
            end
    state = state |> move_ball()
    state |> Helper.change_ball_position()
    {:noreply, state}
  end

  def handle_info(:move, state)
  when (state.x >= @vp_size.width - 60 and state.dxd == 1) do
    state = %{state | dx: 1, dy: 1..10 |> Enum.random, dxd: 0}
    |> move_ball()
    state |> Helper.change_ball_position()
    {:noreply, state}
  end


  def handle_info(:move, state) do
    state = state |> move_ball()
    state |> Helper.change_ball_position()
    {:noreply, state}
  end

  def generate_state() do
    %@ball{
      dx: 0.5,
      dy: 1..10 |> Enum.random,
      dxd: 0..1 |> Enum.random,
      dyd: 0..1 |> Enum.random,
      x: (@vp_size.width - 20) / 2,
      y: (@vp_size.height - 20) / 2
    }
  end

  def get_y do
    GenServer.call(@ball, :y)
  end

  def next_move(val, 1) do
    val
  end

  def next_move(val, 0) do
    -val
  end


  def move_ball(state) do
    %{state | x: state.x + next_move(state.dx, state.dxd), y: state.y + next_move(state.dy, state.dyd) / 10}
  end

end
